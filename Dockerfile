FROM nginx:alpine

# COPY . /usr/share/nginx/html

#COPY css/ /usr/share/nginx/html/css
#COPY js/ /usr/share/nginx/html/js
#COPY img/ /usr/share/nginx/html/img
#COPY index.html  /usr/share/nginx/html 

WORKDIR /usr/share/nginx/html

COPY css css
COPY js js
COPY img img
COPY index.html .
